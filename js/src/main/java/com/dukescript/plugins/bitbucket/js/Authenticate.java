/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dukescript.plugins.bitbucket.js;

import net.java.html.js.JavaScriptBody;

/**
 *
 * @author antonepple
 */
public class Authenticate {

    @JavaScriptBody(args = {"username", "password"}, body = "console.log(username+':'+password);return btoa(username+':'+password)")
    public static native String getAuthenticationString(String username, String password);
}
