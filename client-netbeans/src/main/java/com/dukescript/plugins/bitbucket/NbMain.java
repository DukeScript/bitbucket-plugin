package com.dukescript.plugins.bitbucket;

import org.netbeans.api.htmlui.OpenHTMLRegistration;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;

public class NbMain {
    private NbMain() {
    }
    
    @ActionID(
        category = "Tools",
        id = "com.dukescript.plugins.bitbucket.OpenPage"
    )
    @OpenHTMLRegistration(
        url="index.html",
        displayName = "Open Your Page",
        iconBase = "com/dukescript/plugins/bitbucket/icon.png"
    )
    @ActionReferences({
        @ActionReference(path = "Menu/Window"),
        @ActionReference(path = "Toolbars/Issues")
    })
    public static void onPageLoad() throws Exception {
       
        Main.onPageLoad();
    }
}
