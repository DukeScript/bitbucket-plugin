package com.dukescript.plugins.bitbucket;

import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Repos", properties = {
    @Property(name = "size", type = int.class),
    @Property(name = "next", type = String.class),
    @Property(name = "values", type = Repository.class, array = true)

})
public class RepositoryVMD {

    @Model(className = "Repository", properties = {
        @Property(name = "name", type = String.class),
        @Property(name = "uuid", type = String.class)
    })
    static class RepoVMD {
    }

}
