/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dukescript.plugins.bitbucket;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Issues", properties = {
    @Property(name = "values", type = Issue.class, array = true),
    @Property(name = "next", type = String.class),
    @Property(name = "previous", type = String.class)
})
public class IssuesVMD {

    @Model(className = "Issue", properties = {
        @Property(name = "title", type = String.class),
        @Property(name = "kind", type = Kind.class),
        @Property(name = "state", type = String.class),
        @Property(name = "priority", type = String.class),
        @Property(name = "votes", type = int.class),
        @Property(name = "assignee", type = User.class),
        @Property(name = "created_on", type = String.class),
        @Property(name = "updated_on", type = String.class),})
    static class IssueVMD {

        @ComputedProperty
        public static String kindCss(Kind kind) {
            if (kind == Kind.bug){
                return "icon-bug";
            }
            else if (kind == Kind.enhancement){
                return "icon-external-link-sign";
            }
            else if (kind == Kind.proposal){
                return "icon-expand-alt";
            }
            else {
                return "icon-check";
            }
        }

        
        @ComputedProperty
        public static String stateCss(Kind kind) {
            if (kind == Kind.bug){
                return "icon-bug";
            }
            else if (kind == Kind.enhancement){
                return "icon-external-link-sign";
            }
            else if (kind == Kind.proposal){
                return "icon-expand-alt";
            }
            else {
                return "icon-check";
            }
        }

    }

    @Model(className = "User", properties = {
        @Property(name = "username", type = String.class)
    })
    static class UserVMD {
    }
}
