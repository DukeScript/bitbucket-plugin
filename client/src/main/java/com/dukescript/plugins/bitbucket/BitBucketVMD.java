package com.dukescript.plugins.bitbucket;

import com.dukescript.plugins.bitbucket.js.Authenticate;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.OnReceive;
import net.java.html.json.Property;

@Model(className = "BitBucket", targetId = "", properties = {
    @Property(name = "userName", type = String.class),
    @Property(name = "password", type = String.class),
    @Property(name = "message", type = String.class),
    @Property(name = "sortField", type = TableHeader.class),
    @Property(name = "sortAscending", type = boolean.class),
    @Property(name = "connected", type = boolean.class),
    @Property(name = "selectedRepo", type = String.class),
    @Property(name = "repositories", type = Repository.class, array = true),
    @Property(name = "issues", type = Issue.class, array = true),
    @Property(name = "nextIssuesURL", type = String.class),
    @Property(name = "previousIssuesURL", type = String.class),
    @Property(name = "tableHeaders", type = TableHeader.class, array = true)
})
final class BitBucketVMD {

    private static BitBucket ui;

    /**
     * Called when the page is ready.
     */
    static void onPageLoad() throws Exception {

        ui = new BitBucket();
        TableHeader created = new TableHeader("created_on", "Created", "Created", "xl");
        ui.getTableHeaders().add(new TableHeader("title", "Title", "Title", "sm"));
        ui.getTableHeaders().add(new TableHeader("kind", "Issue Type", "T", "sm"));
        ui.getTableHeaders().add(new TableHeader("priority", "Priority", "P", "sm"));
        ui.getTableHeaders().add(new TableHeader("state", "State", "S", "sm"));
        ui.getTableHeaders().add(new TableHeader("votes", "Votes", "V", "md"));
        ui.getTableHeaders().add(new TableHeader("assignee", "Assignee", "Assignee", "lg"));
        ui.getTableHeaders().add(created);
        ui.getTableHeaders().add(new TableHeader("updated_on", "Updated", "Updated", "xl"));
        ui.setSortField(created);
        ui.setSortAscending(false);
        ui.applyBindings();
    }

    @ComputedProperty()
    public static String sortCss(boolean sortAscending) {
        if (sortAscending) {
            return "icon-sort-down sort-icon";            
        }
        return "icon-sort-up sort-icon";
    }

    @OnPropertyChange(value = "selectedRepo")
    public static void getIssues(BitBucket model) {
        model.loadIssues("https://api.bitbucket.org/2.0/repositories/", model.getUserName(), model.getSelectedRepo(), getSortString(model), "state=\"new\"", Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
    }

    @Function
    public static void connect(BitBucket model) {
        model.connect("https://api.bitbucket.org/2.0/repositories/", model.getUserName(), Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
    }

    @Function
    public static void sort(BitBucket model, TableHeader data) {

        if (model.getSortField().equals(data)) {
            model.setSortAscending(!model.isSortAscending());
        } else {
            model.setSortField(data);
            model.setSortAscending(true);
        }
        model.loadIssues("https://api.bitbucket.org/2.0/repositories/", model.getUserName(), model.getSelectedRepo(), getSortString(model), "state=\"new\"", Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
    }

    @Function
    public static void nextIssues(BitBucket model) {
        model.loadNextIssues(model.getNextIssuesURL(), Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
    }

    @Function
    public static void previousIssues(BitBucket model) {
        model.loadNextIssues(model.getPreviousIssuesURL(), Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
    }

    @OnReceive(url = "{url}{name}?q=has_issues+%3D+true", headers = {"Authorization: Basic {auth}"}, onError = "cannotConnect")
    public static void connect(BitBucket model, Repos repos) {
        model.setConnected(true);
        model.setMessage(null);
        model.getRepositories().clear();
        model.getRepositories().addAll(repos.getValues());
        System.out.println("next " + repos.getNext());
        if (null != repos.getNext()) {
            model.addRepositories(repos.getNext(), Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
        }
    }

    @OnReceive(url = "{url}", headers = {"Authorization: Basic {auth}"}, onError = "cannotConnect")
    public static void addRepositories(BitBucket model, Repos repos) {
        model.setMessage(null);
        model.getRepositories().addAll(repos.getValues());
        if (null != repos.getNext()) {
            model.addRepositories(repos.getNext(), Authenticate.getAuthenticationString(model.getUserName(), model.getPassword()));
        }
    }

    @OnReceive(url = "{url}{name}/{slug}/issues?sort={sort}&q={query}", headers = {"Authorization: Basic {auth}"}, onError = "cannotConnect")
    public static void loadIssues(BitBucket model, Issues issues) {
        model.getIssues().clear();
        model.getIssues().addAll(issues.getValues());
        model.setNextIssuesURL(issues.getNext());
        model.setPreviousIssuesURL(issues.getPrevious());
    }

    @OnReceive(url = "{url}", headers = {"Authorization: Basic {auth}"}, onError = "cannotConnect")
    public static void loadNextIssues(BitBucket model, Issues issues) {
        model.getIssues().clear();
        model.getIssues().addAll(issues.getValues());
        model.setNextIssuesURL(issues.getNext());
        model.setPreviousIssuesURL(issues.getPrevious());
    }

    static void cannotConnect(BitBucket data, Exception ex) {
        data.setMessage("Cannot connect " + ex.getMessage());
    }

    private static String getSortString(BitBucket model) {
        return (model.isSortAscending() ? "" : "-") + model.getSortField().getName();
    }

    @Model(className = "TableHeader", properties = {
        @Property(name = "name", type = String.class),
        @Property(name = "title", type = String.class),
        @Property(name = "displayName", type = String.class),
        @Property(name = "visible", type = String.class)
    })
    final static class TableHeaderVMD {

    }

}
